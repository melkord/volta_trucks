# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). The API layer is built using https://mockapi.io/ to simulate a backend API. It's provided with `.nvmrc` file to use the correct node version.

## Getting started

In the project directory, run the following commands:

- `nvm install && nvm use`
- `npm install`
- `npm start`

The app will runs in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Other commands

### `npm test`

Launches the test runner in the interactive watch mode. You can get the coverage report by running `npm test -- --coverage --watchAll=false`

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

## Online demo

The app is deployed on Netlify: https://truck-dashboard.netlify.app/.

## Caveats

- The app use a API mock service, maybe sometimes the API response is slow or give an error for reaching the limit of requests.
- The online demo is deployed on Netlify, it's a free service, so the first time you access the app, it may take a while to load or it could be offline with no reason. Please try again later.
- The app is a MVP, so there could be some bugs or unexpected behaviors.
- The tests are not complete, I just added some tests for the main parts or to show how I would test the app.
