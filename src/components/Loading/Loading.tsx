import React from 'react';
import styled, { keyframes } from 'styled-components';

interface SpinnerProps {
  size?: string;
  color?: string;
}

const spin = keyframes`
  to { transform: rotate(360deg); }
`;

const SpinnerWrapper = styled.div<SpinnerProps>`
  display: inline-block;
  width: ${(props) => props.size || '40px'};
  height: ${(props) => props.size || '40px'};
  border: 4px solid rgba(0, 0, 0, 0.1);
  border-left-color: ${(props) => props.color || '#3498db'};
  border-radius: 50%;
  animation: ${spin} 1s linear infinite;
`;

const Spinner: React.FC<SpinnerProps> = ({ size, color }) => {
  return <SpinnerWrapper size={size} color={color}></SpinnerWrapper>;
};

export default Spinner;
