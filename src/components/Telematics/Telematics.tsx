import React, { useContext, useMemo } from 'react';
import { ApiContext } from '../../context/apiContext';
import {
  Container,
  TelematicVisibleItem,
  TelematicsItem,
  TelematicsItemButton,
  TelematicsItemList,
  TelematicsVisibleItemList,
} from './Telematics.style';

type TelematicsProps = {};

const Telematics: React.FC<TelematicsProps> = () => {
  const {
    state: {
      boardData: { telemetics },
    },
    toggleTelematics,
  } = useContext(ApiContext);

  const telematicsToShow = useMemo(
    () => telemetics.filter(({ visible }) => visible),
    [telemetics]
  );

  return (
    <Container>
      <TelematicsVisibleItemList>
        <h3>Telematics visible</h3>
        {telematicsToShow.map(({ name, value, id }) => (
          <TelematicVisibleItem key={id}>
            <div>
              <h3>{name}:</h3> <p>{value}</p>
            </div>
          </TelematicVisibleItem>
        ))}
      </TelematicsVisibleItemList>
      <TelematicsItemList>
        <h3>Telematics available</h3>
        {telemetics.map(({ name, id, visible }) => {
          const buttonLabel = visible ? '>' : '<';
          return (
            <TelematicsItem key={id}>
              <p>
                <TelematicsItemButton
                  onClick={() => {
                    toggleTelematics(id);
                  }}
                  $isVisible={visible}
                >
                  {buttonLabel}
                </TelematicsItemButton>
                {name}
              </p>
            </TelematicsItem>
          );
        })}
      </TelematicsItemList>
    </Container>
  );
};

export default Telematics;
