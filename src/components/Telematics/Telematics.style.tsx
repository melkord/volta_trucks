import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
`;

export const TelematicsVisibleItemList = styled.div`
  flex: 3;
  padding: 10px;
  height: 100%;
  align-items: center;
`;

export const TelematicVisibleItem = styled.div`
  height: 60px;
  margin-bottom: 10px;
  background-color: #f2f2f2;
`;

export const TelematicsItemList = styled.div`
  flex: 2;
  padding: 10px;
  height: 100%;
`;

export const TelematicsItem = styled.div`
  padding: 10px;
  height: 50px;
  margin-bottom: 10px;
  background-color: #e0e0e0;

  p > button {
    margin-right: 10px;
  }
`;

export const TelematicsItemButton = styled.button<{ $isVisible: boolean }>`
  padding: 10px;
  margin-right: 10px;
  background-color: ${(props) => (props.$isVisible ? '#d42c48' : '#269404')};
`;
