import React from 'react';

type TelematicItemProps = {
    name: string
    value: string
}

const TelematicItem: React.FC<TelematicItemProps> = ({ name, value }) => {
    return (
        <div>
            <p>Name: {name}</p>
            <p>Value: {value}</p>
        </div>
    );
}

export default TelematicItem;
