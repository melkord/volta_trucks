import { render, fireEvent, screen } from '@testing-library/react';
import { ApiContext, ApiContextProps } from '../../context/apiContext';
import RoadConditions from './RoadConditions';

const mockContext: ApiContextProps = {
  state: {
    boardData: {
      roadConditions: [
        {
          id: '1',
          name: 'Road Condition 1',
          description: 'Desc Road Condition 1',
        },
        {
          id: '2',
          name: 'Road Condition 2',
          description: 'Desc Road Condition 2',
        },
      ],
      reportedRoadConditions: [
        { id: '1', road_condition_id: '1', latitude: '10', longitude: '20' },
        { id: '2', road_condition_id: ' 2', latitude: '30', longitude: '40' },
      ],
      telemetics: [],
      trips: [],
    },
    isAppLoading: false,
  },
  toggleTelematics: () => {},
  addTrip: () => Promise.resolve(),
  completeTrip: () => Promise.resolve(),
  recordRoadCondition: jest.fn(),
};

describe('RoadConditions', () => {
  it('renders the table with reported road conditions', () => {
    render(
      <ApiContext.Provider value={mockContext}>
        <RoadConditions />
      </ApiContext.Provider>
    );

    expect(screen.getByText('Report Road Condition 1')).toBeInTheDocument();
    expect(screen.getByText('Report Road Condition 2')).toBeInTheDocument();
    expect(screen.getByText('10,20')).toBeInTheDocument();
    expect(screen.getByText('30,40')).toBeInTheDocument();
  });

  it('renders the report button for each road condition', () => {
    render(
      <ApiContext.Provider value={mockContext}>
        <RoadConditions />
      </ApiContext.Provider>
    );

    expect(screen.getByText('Report Road Condition 1')).toBeInTheDocument();
    expect(screen.getByText('Report Road Condition 2')).toBeInTheDocument();
  });

  it('calls recordRoadCondition when a report button is clicked', () => {
    render(
      <ApiContext.Provider value={mockContext}>
        <RoadConditions />
      </ApiContext.Provider>
    );

    fireEvent.click(screen.getByText('Report Road Condition 1'));
    expect(mockContext.recordRoadCondition).toHaveBeenCalledWith('1');

    fireEvent.click(screen.getByText('Report Road Condition 2'));
    expect(mockContext.recordRoadCondition).toHaveBeenCalledWith('2');
  });
});
