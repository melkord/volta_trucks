import React, { useContext, useMemo } from 'react';

import { ApiContext } from '../../context/apiContext';
import {
  Container,
  TableContainer,
  RoadConditionsTable,
  TableHeader,
  TableRow,
  TableCell,
  Button,
} from './RoadConditions.style';

type RoadConditionMap = {
  [key: string]: string;
};

const RoadConditions: React.FC = () => {
  const {
    state: {
      boardData: { roadConditions, reportedRoadConditions },
    },
    recordRoadCondition,
  } = useContext(ApiContext);

  const roadConditionMap = useMemo(
    () =>
      roadConditions.reduce((acc, current) => {
        return {
          ...acc,
          [current.id]: current.name,
        };
      }, {} as RoadConditionMap),
    [roadConditions]
  );

  return (
    <Container>
      <TableContainer>
        <RoadConditionsTable>
          <thead>
            <tr>
              <TableHeader>Name</TableHeader>
              <TableHeader>Coordinate</TableHeader>
            </tr>
          </thead>
          <tbody>
            {reportedRoadConditions.map((reportedRoadConditions) => (
              <TableRow key={reportedRoadConditions.id}>
                <TableCell>
                  {roadConditionMap[reportedRoadConditions.road_condition_id] ??
                    reportedRoadConditions.id}
                </TableCell>
                <TableCell>{`${reportedRoadConditions.latitude},${reportedRoadConditions.longitude}`}</TableCell>
              </TableRow>
            ))}
          </tbody>
        </RoadConditionsTable>
      </TableContainer>
      <div>
        {roadConditions.map((roadCondition) => (
          <div key={roadCondition.id}>
            <p>
              <Button onClick={() => recordRoadCondition(roadCondition.id)}>
                Report {roadCondition.name}
              </Button>
            </p>
          </div>
        ))}
      </div>
    </Container>
  );
};

export default RoadConditions;
