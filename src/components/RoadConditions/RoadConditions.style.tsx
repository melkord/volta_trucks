import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
`;

export const TableContainer = styled.div`
  flex: 4;
  margin-right: 10px;
`;

export const RoadConditionsTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-bottom: 20px;
`;

export const TableHeader = styled.th`
  border: 1px solid #ddd;
  padding: 8px;
  text-align: left;
`;

export const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f9f9f9;
  }
`;

export const TableCell = styled.td`
  border: 1px solid #ddd;
  padding: 8px;
  text-align: left;
`;

export const Button = styled.button`
  padding: 10px 20px;
  margin-left: 10px;
`;
