import styled from 'styled-components';

export const TableContainer = styled.div`
  margin: 20px;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-bottom: 20px;
`;

export const TableHeader = styled.th`
  border: 1px solid #ddd;
  padding: 8px;
  text-align: left;
`;

export const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f9f9f9;
  }
`;

export const TableCell = styled.td`
  border: 1px solid #ddd;
  padding: 8px;
`;

export const FormContainer = styled.div`
  margin-bottom: 20px;
`;

export const FormInput = styled.input`
  padding: 10px;
  margin-right: 10px;
`;

export const FormButton = styled.button`
  padding: 10px 20px;
`;

export const ValidationMessage = styled.p`
  color: red;
`;
