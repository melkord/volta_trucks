import React, { useCallback, useContext, useMemo, useState } from 'react';

import { ApiContext } from '../../context/apiContext';
import { Trip } from '../../type';
import {
  FormButton,
  FormContainer,
  FormInput,
  Table,
  TableCell,
  TableContainer,
  TableHeader,
  TableRow,
  ValidationMessage,
} from './Trips.style';

import {
  validateCompleteTrip,
  validateAddTrip,
} from '../../helpers/validation';

const EMPTY_TRIP: Trip = {
  id: '',
  driver_name: '',
  start_date: '',
  end_date: '',
  km: 0,
};

const Trips: React.FC = () => {
  const {
    state: {
      boardData: { trips },
    },
    addTrip,
    completeTrip,
  } = useContext(ApiContext);

  const [validationMessage, setValidationMessage] = useState<
    string | undefined
  >(undefined);

  const tripFormToComplete = useMemo(() => {
    return trips.find(({ end_date }) => end_date === '');
  }, [trips]);

  const [tripForm, setTripForm] = useState<Trip>(
    tripFormToComplete ? tripFormToComplete : EMPTY_TRIP
  );

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const { name, value } = e.target;
      setValidationMessage(undefined);
      setTripForm((prevRecord) => ({ ...prevRecord, [name]: value }));
    },
    []
  );

  const addTripCallback = useCallback(
    (tripForm: Trip) => {
      const validationError = validateAddTrip(tripForm);
      if (validationError === '') {
        addTrip(tripForm).then(() => {
          setTripForm(
            validateCompleteTrip(tripForm) !== '' ? tripForm : EMPTY_TRIP
          );
        });
      } else {
        setValidationMessage(validationError);
      }
    },
    [addTrip]
  );
  const completeTripCallback = useCallback(
    (tripForm: Trip) => {
      const validationError = validateCompleteTrip(tripForm);
      if (validationError === '') {
        completeTrip(tripForm).then(() => {
          setTripForm(EMPTY_TRIP);
        });
      } else {
        setValidationMessage(validationError);
      }
    },
    [completeTrip]
  );
  return (
    <TableContainer>
      <FormContainer>
        <FormInput
          type="text"
          placeholder="Driver Name"
          name="driver_name"
          value={tripForm.driver_name}
          onChange={handleInputChange}
        />
        <FormInput
          type="text"
          placeholder="Start Date"
          name="start_date"
          value={tripForm.start_date}
          onChange={handleInputChange}
        />
        <FormInput
          type="text"
          placeholder="End Date"
          name="end_date"
          value={tripForm.end_date}
          onChange={handleInputChange}
        />
        <FormInput
          type="number"
          placeholder="Km"
          name="km"
          value={tripForm.km}
          onChange={handleInputChange}
        />
        <FormButton
          onClick={() => {
            if (tripFormToComplete !== undefined) {
              completeTripCallback(tripForm);
            } else {
              addTripCallback(tripForm);
            }
          }}
        >
          {!!tripFormToComplete ? 'Complete Trip' : 'Add Trip'}
        </FormButton>
        {validationMessage && (
          <ValidationMessage>{validationMessage}</ValidationMessage>
        )}
      </FormContainer>

      <Table>
        <thead>
          <tr>
            <TableHeader>Driver Name</TableHeader>
            <TableHeader>Start Date</TableHeader>
            <TableHeader>End Date</TableHeader>
            <TableHeader>Km</TableHeader>
          </tr>
        </thead>
        <tbody>
          {trips.map((trip) => (
            <TableRow key={trip.id}>
              <TableCell>{trip.driver_name}</TableCell>
              <TableCell>{trip.start_date}</TableCell>
              <TableCell>{trip.end_date}</TableCell>
              <TableCell>{trip.km}</TableCell>
            </TableRow>
          ))}
        </tbody>
      </Table>
    </TableContainer>
  );
};

export default Trips;
