import { Action, reducer } from './apiContext';

describe('Reducer', () => {
  const initialState = {
    boardData: {
      telemetics: [],
      trips: [],
      roadConditions: [],
      reportedRoadConditions: [],
    },
    isAppLoading: false,
  };

  it('should set board data', () => {
    const payload = {
      telemetics: [{ id: '1', visible: true, name: 'name', value: 'value' }],
      trips: [
        {
          id: '1',
          tripData: 'tripData',
          driver_name: 'driver_name',
          start_date: 'start_date',
          end_date: 'end_date',
          km: 1,
        },
      ],
      roadConditions: [{ id: '1', name: 'name', description: 'description' }],
      reportedRoadConditions: [
        {
          id: '1',
          roadConditionData: 'roadConditionData',
          road_condition_id: '1',
          longitude: 'longitude',
          latitude: 'latitude',
        },
      ],
    };

    const action = { type: 'SET_BOARD_DATA' as const, payload };
    const newState = reducer(initialState, action);

    expect(newState.boardData).toEqual(payload);
  });

  it('should set loading', () => {
    const payload = true;

    const action = { type: 'SET_LOADING' as const, payload };
    const newState = reducer(initialState, action);

    expect(newState.isAppLoading).toEqual(payload);
  });

  it('should toggle telematics visibility', () => {
    const payload = '1';

    const initialStateWithTelematics = {
      ...initialState,
      boardData: {
        ...initialState.boardData,
        telemetics: [
          { id: payload, visible: true, name: 'name', value: 'value' },
        ],
      },
    };

    const action = { type: 'TOGGLE_TELEMETIC' as const, payload };
    const newState = reducer(initialStateWithTelematics, action);

    expect(newState.boardData.telemetics[0].visible).toEqual(false);
  });

  it('should add a trip', () => {
    const payload = {
      id: '1',
      tripData: 'tripData',
      driver_name: 'driver_name',
      start_date: 'start_date',
      end_date: 'end_date',
      km: 1,
    };

    const action = { type: 'ADD_TRIP' as const, payload };
    const newState = reducer(initialState, action);

    expect(newState.boardData.trips).toContainEqual(payload);
  });

  it('should complete a trip', () => {
    const tripToUpdate = {
      id: '1',
      driver_name: 'driver_name',
      start_date: 'start_date',
      end_date: '',
      km: 0,
    };
    const payload = {
      id: '1',
      driver_name: 'driver_name',
      start_date: 'start_date',
      end_date: 'end_date',
      km: 1,
    };

    const initialStateWithTrip = {
      ...initialState,
      boardData: {
        ...initialState.boardData,
        trips: [tripToUpdate],
      },
    };

    const action = { type: 'COMPLETE_TRIP' as const, payload };
    const newState = reducer(initialStateWithTrip, action);

    expect(newState.boardData.trips).toContainEqual(payload);
  });

  it('should report a road condition', () => {
    const payload = {
      id: '1',
      roadConditionData: 'roadConditionData',
      road_condition_id: '1',
      longitude: 'longitude',
      latitude: 'latitude',
    };

    const action = { type: 'REPORT_ROAD_CONDITION' as const, payload };
    const newState = reducer(initialState, action);

    expect(newState.boardData.reportedRoadConditions).toContainEqual(payload);
  });

  it('should return the initial state for an unknown action type', () => {
    const action = { type: 'UNKNOWN_ACTION' };
    const newState = reducer(initialState, action as Action);

    expect(newState).toEqual(initialState);
  });
});
