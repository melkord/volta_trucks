import React, {
  createContext,
  useCallback,
  useEffect,
  useReducer,
} from 'react';

import {
  getAllReportedRoadConditions,
  getAllRoadConditions,
  getAllTelematics,
  getAllTrips,
  postReportedRoadCondition,
  postTrip,
  updateTelematic,
  updateTrip,
} from '../helpers/api';
import { ReportedRoadCondition, RoadCondition, Telematic, Trip } from '../type';
import { getCoordinates } from '../helpers/coordinates';

type State = {
  boardData: {
    telemetics: Telematic[];
    trips: Trip[];
    roadConditions: RoadCondition[];
    reportedRoadConditions: ReportedRoadCondition[];
  };
  isAppLoading: boolean;
};

export type ApiContextProps = {
  state: State;
  toggleTelematics: (id: string) => void;
  addTrip: (newTrip: Trip) => Promise<void>;
  completeTrip: (trip: Trip) => Promise<void>;
  recordRoadCondition: (roadConditionId: string) => Promise<void>;
};

const defaultApiContext: ApiContextProps = {
  state: {
    boardData: {
      telemetics: [],
      trips: [],
      roadConditions: [],
      reportedRoadConditions: [],
    },
    isAppLoading: false,
  },
  toggleTelematics: () => {},
  addTrip: () => Promise.resolve(),
  completeTrip: () => Promise.resolve(),
  recordRoadCondition: () => Promise.resolve(),
};

export type Action =
  | { type: 'SET_BOARD_DATA'; payload: State['boardData'] }
  | { type: 'SET_LOADING'; payload: boolean }
  | { type: 'TOGGLE_TELEMETIC'; payload: string }
  | { type: 'ADD_TRIP'; payload: Trip }
  | { type: 'COMPLETE_TRIP'; payload: Trip }
  | { type: 'REPORT_ROAD_CONDITION'; payload: ReportedRoadCondition };

export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'SET_BOARD_DATA':
      return { ...state, boardData: action.payload };
    case 'SET_LOADING':
      return { ...state, isAppLoading: action.payload };
    case 'TOGGLE_TELEMETIC':
      return {
        ...state,
        boardData: {
          ...state.boardData,
          telemetics: state.boardData.telemetics.map((telematic) =>
            telematic.id === action.payload
              ? { ...telematic, visible: !telematic.visible }
              : telematic
          ),
        },
      };
    case 'ADD_TRIP':
      return {
        ...state,
        boardData: {
          ...state.boardData,
          trips: [...state.boardData.trips, action.payload],
        },
      };
    case 'COMPLETE_TRIP':
      return {
        ...state,
        boardData: {
          ...state.boardData,
          trips: state.boardData.trips.map((trip) =>
            trip.id === action.payload.id ? action.payload : trip
          ),
        },
      };
    case 'REPORT_ROAD_CONDITION':
      return {
        ...state,
        boardData: {
          ...state.boardData,
          reportedRoadConditions: [
            ...state.boardData.reportedRoadConditions,
            action.payload,
          ],
        },
      };
    default:
      return state;
  }
};

export const ApiContext = createContext<ApiContextProps>(defaultApiContext);

const ApiProvider: React.FC<{ children: JSX.Element }> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, defaultApiContext.state);

  useEffect(() => {
    dispatch({ type: 'SET_LOADING', payload: true });

    Promise.all([
      getAllTelematics(),
      getAllTrips(),
      getAllRoadConditions(),
      getAllReportedRoadConditions(),
    ])
      .then(([telemetics, trips, roadConditions, reportedRoadConditions]) => {
        dispatch({
          type: 'SET_BOARD_DATA',
          payload: {
            telemetics,
            trips,
            roadConditions,
            reportedRoadConditions,
          },
        });
      })
      .finally(() => dispatch({ type: 'SET_LOADING', payload: false }));
  }, []);

  const toggleTelematics = useCallback(
    (id: string) => {
      const telematicToUpdate = state.boardData.telemetics.find(
        (telematic) => telematic.id === id
      );
      if (telematicToUpdate) {
        updateTelematic(id, {
          ...telematicToUpdate,
          visible: !telematicToUpdate.visible,
        })
          .then(() => {
            dispatch({ type: 'TOGGLE_TELEMETIC', payload: id });
          })
          .catch((err: unknown) => {
            console.error(err);
          });
      } else {
        console.error('Telematic not found');
      }
    },
    [state.boardData.telemetics]
  );

  const addTrip = useCallback(
    (newTrip: Trip) => {
      return postTrip(newTrip)
        .then(() => {
          dispatch({ type: 'ADD_TRIP', payload: newTrip });
        })
        .catch((err: unknown) => {
          console.error(err);
        });
    },
    [dispatch]
  );

  const completeTrip = useCallback((trip: Trip) => {
    return updateTrip(trip.id, trip)
      .then(() => {
        dispatch({ type: 'COMPLETE_TRIP', payload: trip });
      })
      .catch((err: unknown) => {
        console.error(err);
      });
  }, []);

  const recordRoadCondition = useCallback((roadConditionId: string) => {
    return getCoordinates()
      .then((coordinates) => {
        return postReportedRoadCondition({
          id: '-1',
          road_condition_id: roadConditionId,
          ...coordinates,
        });
      })
      .then((reportedRoadCondition) => {
        dispatch({
          type: 'REPORT_ROAD_CONDITION',
          payload: reportedRoadCondition,
        });
      })
      .catch((err: unknown) => {
        console.error(err);
      });
  }, []);

  return (
    <ApiContext.Provider
      value={{
        state,
        toggleTelematics,
        addTrip,
        completeTrip,
        recordRoadCondition,
      }}
    >
      {children}
    </ApiContext.Provider>
  );
};

export default ApiProvider;
