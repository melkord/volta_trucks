import { validateAddTrip, validateCompleteTrip } from './validation';

describe('validateAddTrip', () => {
  it('should return an empty string if driver_name and start_date are provided', () => {
    const partialTrip = {
      driver_name: 'John Doe',
      start_date: '2022-01-01',
    };

    const result = validateAddTrip(partialTrip);

    expect(result).toBe('');
  });

  it('should return an error message if driver_name is missing', () => {
    const partialTrip = {
      start_date: '2022-01-01',
    };

    const result = validateAddTrip(partialTrip);

    expect(result).toBe('Driver name and start date are mandatory');
  });

  it('should return an error message if start_date is missing', () => {
    const partialTrip = {
      driver_name: 'John Doe',
    };

    const result = validateAddTrip(partialTrip);

    expect(result).toBe('Driver name and start date are mandatory');
  });
});

describe('validateCompleteTrip', () => {
  it('should return an empty string if end_date and km are provided', () => {
    const partialTrip = {
      end_date: '2022-01-02',
      km: 100,
    };

    const result = validateCompleteTrip(partialTrip);

    expect(result).toBe('');
  });

  it('should return an error message if end_date is missing', () => {
    const partialTrip = {
      km: 100,
    };

    const result = validateCompleteTrip(partialTrip);

    expect(result).toBe('End date and Kilometer are mandatory');
  });

  it('should return an error message if km is missing', () => {
    const partialTrip = {
      end_date: '2022-01-02',
    };

    const result = validateCompleteTrip(partialTrip);

    expect(result).toBe('End date and Kilometer are mandatory');
  });
});
