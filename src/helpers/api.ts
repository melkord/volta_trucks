import {
  ReportedRoadCondition,
  Resource,
  RoadCondition,
  Telematic,
  Trip,
} from '../type';

const BASE_URL = 'https://65166cd509e3260018c9c0ac.mockapi.io';

async function getAll<T extends Resource>(resource: string): Promise<T[]> {
  const response = await fetch(`${BASE_URL}/${resource}`);
  const data = await response.json();
  return data;
}

async function post<T extends Resource>(resource: string, data: T): Promise<T> {
  const response = await fetch(`${BASE_URL}/${resource}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  const responseData = await response.json();
  return responseData;
}

async function update<T extends Resource>(
  resource: string,
  id: string,
  data: T
): Promise<T> {
  const response = await fetch(`${BASE_URL}/${resource}/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  const responseData = await response.json();
  return responseData;
}

export async function getAllTelematics(): Promise<Telematic[]> {
  return getAll<Telematic>('telematics');
}

export async function updateTelematic(
  id: string,
  data: Telematic
): Promise<Telematic> {
  return update<Telematic>('telematics', id, data);
}
export async function getAllTrips(): Promise<Trip[]> {
  return getAll<Trip>('trips');
}
export async function postTrip(data: Trip): Promise<Trip> {
  return post<Trip>('trips', data);
}

export async function updateTrip(id: string, data: Trip): Promise<Trip> {
  return update<Trip>('trips', id, data);
}
export async function getAllRoadConditions(): Promise<RoadCondition[]> {
  return getAll<RoadCondition>('road_conditions');
}
export async function getAllReportedRoadConditions(): Promise<
  ReportedRoadCondition[]
> {
  return getAll<ReportedRoadCondition>('reported_road_conditions');
}
export async function postReportedRoadCondition(
  data: ReportedRoadCondition
): Promise<ReportedRoadCondition> {
  return post<ReportedRoadCondition>('reported_road_conditions', data);
}
