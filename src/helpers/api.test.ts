import fetchMock from 'jest-fetch-mock';
import {
  getAllTelematics,
  getAllTrips,
  getAllRoadConditions,
  getAllReportedRoadConditions,
  postTrip,
  postReportedRoadCondition,
  updateTelematic,
  updateTrip,
} from './api';

fetchMock.enableMocks();

const BASE_URL = 'https://65166cd509e3260018c9c0ac.mockapi.io';

describe('getAllTelematics', () => {
  it('should fetch data from the correct URL', async () => {
    fetchMock.mockResponseOnce(JSON.stringify([]));

    await getAllTelematics();

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/telematics`);
  });

  it('should return the correct data', async () => {
    const mockData = [{ id: '1', name: 'Telematic 1' }];
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await getAllTelematics();

    expect(result).toEqual(mockData);
  });
});

describe('updateTelematic', () => {
  it('should send a PUT request to the correct URL with the correct data', async () => {
    const mockData = {
      id: '1',
      name: 'Telematic 1',
      value: 'value',
      visible: true,
    };
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await updateTelematic('1', mockData);

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/telematics/1`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mockData),
    });
    expect(result).toEqual(mockData);
  });
});

describe('getAllTrips', () => {
  it('should fetch data from the correct URL', async () => {
    fetchMock.mockResponseOnce(JSON.stringify([]));

    await getAllTrips();

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/trips`);
  });

  it('should return the correct data', async () => {
    const mockData = [{ id: '1', name: 'Trip 1' }];
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await getAllTrips();

    expect(result).toEqual(mockData);
  });
});

describe('postTrip', () => {
  it('should send a POST request to the correct URL with the correct data', async () => {
    const mockData = {
      id: '1',
      name: 'Trip 1',
      driver_name: 'driver_name',
      start_date: 'start_date',
      end_date: 'end_date',
      km: 1,
    };
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await postTrip(mockData);

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/trips`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mockData),
    });
    expect(result).toEqual(mockData);
  });
});

describe('updateTrip', () => {
  it('should send a PUT request to the correct URL with the correct data', async () => {
    const mockData = {
      id: '1',
      name: 'Trip 1',
      driver_name: 'driver_name',
      start_date: 'start_date',
      end_date: 'end_date',
      km: 1,
    };
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await updateTrip('1', mockData);

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/trips/1`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(mockData),
    });
    expect(result).toEqual(mockData);
  });
});

describe('getAllRoadConditions', () => {
  it('should fetch data from the correct URL', async () => {
    fetchMock.mockResponseOnce(JSON.stringify([]));

    await getAllRoadConditions();

    expect(fetchMock).toHaveBeenCalledWith(`${BASE_URL}/road_conditions`);
  });

  it('should return the correct data', async () => {
    const mockData = [{ id: '1', name: 'Road Condition 1' }];
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await getAllRoadConditions();

    expect(result).toEqual(mockData);
  });
});

describe('getAllReportedRoadConditions', () => {
  it('should fetch data from the correct URL', async () => {
    fetchMock.mockResponseOnce(JSON.stringify([]));

    await getAllReportedRoadConditions();

    expect(fetchMock).toHaveBeenCalledWith(
      `${BASE_URL}/reported_road_conditions`
    );
  });

  it('should return the correct data', async () => {
    const mockData = [{ id: '1', name: 'Reported Road Condition 1' }];
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await getAllReportedRoadConditions();

    expect(result).toEqual(mockData);
  });
});

describe('postReportedRoadCondition', () => {
  it('should send a POST request to the correct URL with the correct data', async () => {
    const mockData = {
      id: '1',
      name: 'Reported Road Condition 1',
      roadConditionData: 'roadConditionData',
      road_condition_id: '1',
      longitude: 'longitude',
      latitude: 'latitude',
    };
    fetchMock.mockResponseOnce(JSON.stringify(mockData));

    const result = await postReportedRoadCondition(mockData);

    expect(fetchMock).toHaveBeenCalledWith(
      `${BASE_URL}/reported_road_conditions`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(mockData),
      }
    );
    expect(result).toEqual(mockData);
  });
});
