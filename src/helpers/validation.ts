import { Trip } from '../type';

const validateAddTrip = (partialTrip: Partial<Trip>): string => {
  return partialTrip.driver_name &&
    partialTrip.driver_name.length > 0 &&
    partialTrip.start_date
    ? ''
    : 'Driver name and start date are mandatory';
};

const validateCompleteTrip = (partialTrip: Partial<Trip>): string => {
  return partialTrip.end_date && partialTrip.km
    ? ''
    : 'End date and Kilometer are mandatory';
};
export { validateAddTrip, validateCompleteTrip };
