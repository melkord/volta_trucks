const getCoordinates = (): Promise<{ latitude: string; longitude: string }> => {
  return new Promise((resolve) => {
    const latitude = Math.random() * 180 - 90;
    const longitude = Math.random() * 360 - 180;
    resolve({ latitude: latitude.toString(), longitude: longitude.toString() });
  });
};

export { getCoordinates };
