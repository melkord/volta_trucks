import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

jest.mock('./components/Telematics', () => () => (
  <div>Telematics Component</div>
));
jest.mock('./components/Trips/Trips', () => () => <div>Trips Component</div>);
jest.mock('./components/RoadConditions', () => () => (
  <div>Road Conditions Component</div>
));
jest.mock('./components/Loading/Loading', () => () => (
  <div>Loading Component</div>
));

describe('App', () => {
  it('should render the Telematics tab by default', () => {
    render(<App />);

    const telematicsTab = screen.getByText('Telematics Component');
    expect(telematicsTab).toBeInTheDocument();

    const tripsTab = screen.queryByText('Trips Component');
    expect(tripsTab).not.toBeInTheDocument();

    const roadConditionsTab = screen.queryByText('Road Conditions Component');
    expect(roadConditionsTab).not.toBeInTheDocument();
  });

  it('should switch tabs when a tab button is clicked', () => {
    render(<App />);

    const tripsTabButton = screen.getByText('Trips');
    fireEvent.click(tripsTabButton);

    const tripsTab = screen.getByText('Trips Component');
    expect(tripsTab).toBeInTheDocument();

    const telematicsTab = screen.queryByText('Telematics Component');
    expect(telematicsTab).not.toBeInTheDocument();

    const roadConditionsTab = screen.queryByText('Road Conditions Component');
    expect(roadConditionsTab).not.toBeInTheDocument();
  });

  it('should switch to the Road Conditions tab when the corresponding button is clicked', () => {
    render(<App />);

    const roadConditionsTabButton = screen.getByText('Road Conditions');
    fireEvent.click(roadConditionsTabButton);

    const roadConditionsTab = screen.getByText('Road Conditions Component');
    expect(roadConditionsTab).toBeInTheDocument();

    const telematicsTab = screen.queryByText('Telematics Component');
    expect(telematicsTab).not.toBeInTheDocument();

    const tripsTab = screen.queryByText('Trips Component');
    expect(tripsTab).not.toBeInTheDocument();
  });
});
