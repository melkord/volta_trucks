import React, { useContext, useMemo, useState } from 'react';
import styled from 'styled-components';
import TelematicsComponent from './components/Telematics';
import TripsComponent from './components/Trips/Trips';
import RoadConditionsComponent from './components/RoadConditions';
import Loading from './components/Loading/Loading';
import { ApiContext } from './context/apiContext';

const AppContainer = styled.div`
  font-family: sans-serif;
  margin: 20px;
  text-align: center;
`;

const TabsContainer = styled.div`
  display: flex;
  margin-bottom: 10px;
`;

const TabButton = styled.div<{ $active: boolean }>`
  flex-grow: 1;
  padding: 10px;
  border: 1px solid #ccc;
  cursor: pointer;
  background-color: ${({ $active }) => ($active ? '#ccc' : 'transparent')};
`;

const TabContent = styled.div`
  padding: 10px;
  border: 1px solid #ccc;
`;

type TabDomain = 'telematics' | 'trips' | 'roadCondition';

type Tab = {
  domain: TabDomain;
  content: JSX.Element;
  title: string;
};

const tabs: Tab[] = [
  {
    domain: 'telematics',
    content: <TelematicsComponent />,
    title: 'Telematics',
  },
  {
    domain: 'trips',
    content: <TripsComponent />,
    title: 'Trips',
  },
  {
    domain: 'roadCondition',
    content: <RoadConditionsComponent />,
    title: 'Road Conditions',
  },
];

const App: React.FC = () => {
  const [visibleTab, setVisibleTab] = useState<TabDomain>('telematics');
  const tabContentToShow = useMemo(
    () => tabs.find(({ domain }) => domain === visibleTab)?.content,
    [visibleTab]
  );

  const {
    state: { isAppLoading },
  } = useContext(ApiContext);

  return (
    <AppContainer>
      <h1>Truck dashboard</h1>
      <TabsContainer>
        {tabs.map((tab, index) => (
          <TabButton
            key={index}
            $active={visibleTab === tab.domain}
            onClick={() => setVisibleTab(tab.domain)}
          >
            {tab.title}
          </TabButton>
        ))}
      </TabsContainer>
      <TabContent>{isAppLoading ? <Loading /> : tabContentToShow}</TabContent>
    </AppContainer>
  );
};

export default App;
