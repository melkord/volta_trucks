export interface Resource {
  id: string;
}

export interface Telematic extends Resource {
  name: string;
  value: string;
  visible: boolean;
}

export interface Trip extends Resource {
  driver_name: string;
  start_date: string;
  end_date: string;
  km: number;
}

export interface RoadCondition extends Resource {
  name: string;
  description: string;
}

export interface ReportedRoadCondition extends Resource {
  road_condition_id: string;
  longitude: string;
  latitude: string;
}
